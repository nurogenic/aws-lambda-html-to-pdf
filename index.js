const wkhtmltopdf = require('wkhtmltopdf');

process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT']

exports.handler = (event, context, callback) => {
    var promises = event.pages.map((page) => {
        return new Promise((resolve, reject) => {
            let html = page.html //new Buffer(page.html, 'base64').toString('utf8')
            wkhtmltopdf(html, page.pdf_options, (err, stream) => {
                if (err)
                    reject(err)
                else {
                    let chunks = []
                    
                    stream.on('data', function(chunk) {
                        chunks.push(chunk);
                    })
                    stream.on('end', function() {
                        var result = Buffer.concat(chunks);
                        resolve(result.toString('base64'))
                    })
                }
            })
        })
    })

    Promise.all(promises)
        .then((pdf_output) => {
            callback(null, { pdf_output })
        })
        .catch(callback)
}